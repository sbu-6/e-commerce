from django.contrib import admin
from .models import Coupon,Product,Gallery


admin.site.register(Coupon)
admin.site.register(Product)
admin.site.register(Gallery)
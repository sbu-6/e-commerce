from django.shortcuts import render, get_object_or_404 , redirect
from django.contrib.auth.decorators import login_required
from order.models import Order , Orderproduct
from django.contrib.auth import get_user_model
from address.models import Address
from address.forms import AddressForm, AddAddressForm
from authentication.forms import UserUpdateForm


@login_required(login_url="account:Login")
def OrderPage(request):
    context = {
        "title":"Profile",
        'orders':None, 
    }
    user = request.user
    orders = Order.objects.filter(
        user = user,
        ordered=False,
    ).first()
    context['orders'] = orders
    return render(request, "profile/profile_detail_order.html", context)

@login_required(login_url="account:Login")
def AllAddressPage(request):
    address = Address.objects.filter(user = request.user)
    context = {
        "title":"My Addresses",
        'address': address
    }
    return render(request, "address/All_address.html",context)

@login_required(login_url="account:Login")
def Add_Address(request):
    form = AddressForm(request.POST,initial={"user": request.user})
    # print(form.errors)
    if form.is_valid():
        form.save()
        return redirect("profile:dashboard")
    context = {
        "title":"Add Address",
        "form": form,
    }
    return render(request, "address/address.html",context)

@login_required(login_url="account:Login")
def DeleteAddress(request,pk):
    get_address = Address.objects.filter(user = request.user)
    address = get_object_or_404(get_address,pk=pk)
    address.delete()
    return redirect("profile:All-Address") 

@login_required(login_url="account:Login")
def UpdateAddress(request,pk):
    address = get_object_or_404(Address , pk=pk)
    if address.user != request.user:
        return redirect('product:Home')
    form = AddAddressForm(request.POST or None,instance=address)
    if form.is_valid():
        form.save()
        return redirect("profile:dashboard")
    context = {
        "title":"UpdateAddress",
        "form": form,
    }
    return render(request, "address/address.html",context)

@login_required(login_url="account:Login")
def UpdateInfo(request):
    form = UserUpdateForm(request.POST or None , instance=request.user)
    if form.is_valid():
        form.save()
        return redirect("profile:dashboard")
    context = {
        "title":"Update Information",
        "form" : form
    }
    
    return render(request, "profile/update_info.html",context)
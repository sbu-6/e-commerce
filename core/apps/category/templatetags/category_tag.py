from django import template
from ..models import Category

register = template.Library()

# @register.inclusion_tag('components/nav.html', takes_context=True)
# def get_category_list(*args, **kwargs):
#     return 'gamer'

@register.simple_tag
def get_categories():
    return Category.objects.get_active()

from django.db import models
from django.conf import settings

ADDRESS_CHOICES = (
    ('B', 'گیرنده'),
    ('S', 'فرستند(فروشگاه)'),
)

class AddressManager(models.Model):
    def get_default(self):
        return self.filter(default=True).first()

class Address(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,verbose_name="کاربر")
    state_address = models.CharField(max_length=100,verbose_name="استان")
    city_address = models.CharField(max_length=100,verbose_name="شهر")
    detail_address = models.TextField(verbose_name="ادرس دقیق")
    zip_code = models.CharField(max_length=100,verbose_name="کد پستی")
    address_type = models.CharField(max_length=1,default="B",blank=True,null=True ,choices=ADDRESS_CHOICES,verbose_name="نوع ادرس")
    default = models.BooleanField(default=False,verbose_name="پیش فرض")

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name="ادرس"
        verbose_name_plural = 'ادرس ها'
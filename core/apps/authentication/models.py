from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user
    
    def create_new_user(self,username, first_name, last_name,phone,email, password, **extra_fields):
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(username=username,first_name=first_name, last_name=last_name,Phone_number=phone,email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)

    def admin(self):
       return self.filter(is_admin=True)


class User(AbstractUser):
    username = models.CharField(verbose_name="نام کاربری",max_length=150,unique=True,)
    email = models.EmailField(blank=False, null=False,verbose_name='ایمیل',unique=True)
    Phone_number = models.CharField(max_length=12,verbose_name='شماره تلفن',blank=False, null=False,unique=True) 
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ['username']
    objects = UserManager()
    
    
    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"

    def get_adverts(self):
       return self.user_advert.all()
    
    def get_active_adverts(self):
        return self.user_advert.active()
    
    def get_deactive_adverts(self):
        return self.user_advert.deactive()
    
    def get_privet_adverts(self):
        return self.user_advert.privet()
    
    def get_public_adverts(self):
        return self.user_advert.public()
    
    def __str__(self):
        return self.username
    
    class Meta:
        ordering = ('first_name', 'last_name')
        verbose_name = 'کاربر'
        verbose_name_plural = 'کاربران'

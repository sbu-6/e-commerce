from django.urls import path
from . import views

app_name = 'product'

urlpatterns =[
    path("",views.ProductList,name='Home'),
    path("<int:page>/",views.ProductList,name='ProductList'),
    path("search/",views.SearchProduct,name='SearchProduct'),
    path("Detail/<slug>/",views.ProductDetail,name="product-detail"),
]
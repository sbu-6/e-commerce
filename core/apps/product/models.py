from django.db import models
from django.db.models import Q
from category.models import Category,Tag
from django.urls import reverse
import os

# ,upload_to="ProductImage/"
def get_file_ext(filepath):
    base_name = os.path.basename(filepath)
    name , ext = os.path.splitext(base_name)
    return  name , ext

def change_file_name(instance, filename):
    name,ext = get_file_ext(filename)
    new_name = f"ProductImage/{instance.id}-{instance.title}{ext}"
    return new_name


class ProductManager(models.Manager):
    def get_by_id(self,product_id):
        return self.get(id=product_id)
    
    def get_exists(self):
        return self.filter(is_existent=True)
    
    def search(self,data):
        lookup = Q(tag__title__iexact=data)|Q(title__icontains=data)
        return self.filter(lookup,is_existent=True).distinct()
    
    

class Product(models.Model):
    poster = models.ImageField(upload_to=change_file_name,default='image_default\default.jpg',verbose_name="عکس")
    slug = models.SlugField(verbose_name="لینک url")
    title = models.CharField(max_length=100,verbose_name="عنوان")
    price = models.FloatField(verbose_name="قیمت")
    discount_price = models.FloatField(blank=True, null=True,verbose_name="تخفیف")
    category = models.ManyToManyField(Category,verbose_name="دسته بندی",related_name="product",blank=True)
    tag = models.ManyToManyField(Tag,verbose_name="تگ ها",related_name="product",blank=True)
    is_existent = models.BooleanField(default=True,verbose_name='موجود/ناموجود')
    description = models.TextField(verbose_name="توضیحات")
    objects = ProductManager()
    
    def __str__(self):
        return self.title
    
    def get_poster(self):
        return self.poster.url
    
    def get_categories(self):
        if self.category.get_active():
            return self.category.get_active()
        return ''
    def get_absolute_url(self):
        return reverse('product:product-detail', kwargs={'slug': self.slug})
    def get_gallery(self):
        return self.gallery.all()


    # def get_add_to_cart_url(self):
    #     return reverse("core:add-to-cart", kwargs={
    #         'slug': self.slug
    #     })

    # def get_remove_from_cart_url(self):
    #     return reverse("core:remove-from-cart", kwargs={
    #         'slug': self.slug
    #     })
    class Meta:
        verbose_name ='محصول'
        verbose_name_plural ='محصولات'


def change_file_name_gallery(instance, filename):
    name,ext = get_file_ext(filename)
    new_name = f"Gallery/{instance.id}-{instance.title}{ext}"
    return new_name


class Gallery(models.Model):
    title = models.CharField(max_length=50,verbose_name="عنوان")
    image = models.ImageField(verbose_name='عکس',upload_to=change_file_name_gallery)
    product = models.ForeignKey(to=Product, on_delete=models.CASCADE,verbose_name='محصول',related_name='gallery')

    def get_image(self):
        return self.image.url
    
    def __str__(self):
        return f"{self.title}"
    class Meta:
        verbose_name ='گالری'
        verbose_name_plural ='گالری'
    
        
class Coupon(models.Model):
    code = models.CharField(max_length=15,verbose_name="کد")
    amount = models.FloatField(verbose_name="میزان تخفیف")

    def __str__(self):
        return self.code
    
    class Meta:
        verbose_name ="کپن"
        verbose_name_plural ="کپن ها"
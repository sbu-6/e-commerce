from django.urls import path
from order.models import Order , Orderproduct
from . import views


app_name = "profile"
urlpatterns = [
    path("",views.OrderPage,name="dashboard"),
    path("UpdateInfo/",views.UpdateInfo,name="Update-Info"),
    path("All-address/",views.AllAddressPage,name="All-Address"),
    path("Add-Address/",views.Add_Address,name="Add-Address"),
    path("Delete-Address/<pk>/",views.DeleteAddress,name="Delete-Address"),
    path("Update-Address/<pk>/",views.UpdateAddress,name="Update-Address"),
]
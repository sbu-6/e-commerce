from django.contrib import admin
from .models import Order , Orderproduct

admin.site.register(Order)

admin.site.register(Orderproduct)
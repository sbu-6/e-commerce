from django.db import models
from product.models import Product , Coupon
from address.models import Address
from django.conf import settings


class Orderproduct(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,verbose_name="کاربر")
    ordered = models.BooleanField(default=False,verbose_name="انجام شده/انجام نشده")
    product = models.ForeignKey(to=Product, on_delete=models.CASCADE,verbose_name="محصول",related_name='order_detail')
    quantity = models.IntegerField(default=1,verbose_name="تعداد")

    def __str__(self):
        return f"{self.quantity} of {self.product.title}"

    def get_total_product_price(self):
        return self.quantity * self.product.price

    def get_total_discount_product_price(self):
        if self.quantity and  self.product.discount_price:
            return self.quantity * self.product.discount_price
        return ''

    def get_amount_saved(self):
        return self.get_total_product_price() - self.get_total_discount_product_price()

    def get_final_price(self):
        if self.product.discount_price:
            return self.get_total_discount_product_price()
        return self.get_total_product_price()
    
    class Meta:
        verbose_name = "جزئیات سفارش"
        verbose_name_plural ="جزئیات سفارش"


class Order(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,verbose_name="کاربر")
    ref_code = models.CharField(max_length=20, blank=True, null=True,verbose_name="کد مرجع")
    products = models.ManyToManyField(Orderproduct,verbose_name="محصولات")
    start_date = models.DateTimeField(auto_now_add=True,verbose_name="تاریخ شروع")
    ordered_date = models.DateTimeField(verbose_name="تاریخ سفارش")
    ordered = models.BooleanField(default=False,verbose_name="انجام شده/ نشده")
    shipping_address = models.ForeignKey(
        Address, related_name='shipping_address', on_delete=models.SET_NULL, blank=True, null=True,verbose_name="ادرس فروشگاه")
    billing_address = models.ForeignKey(
        Address, related_name='billing_address', on_delete=models.SET_NULL, blank=True, null=True,verbose_name="ادرس گیرنده")
    # payment = models.ForeignKey(
    #     'Payment', on_delete=models.SET_NULL, blank=True, null=True,verbose_name="")
    coupon = models.ForeignKey(
        Coupon, on_delete=models.SET_NULL, blank=True, null=True,verbose_name="کد تخفیف")
    being_delivered = models.BooleanField(default=False,verbose_name="اماده ارسال")
    received = models.BooleanField(default=False,verbose_name="دریافت شده/ نشده")
    refund_requested = models.BooleanField(default=False,verbose_name="در خواست مرجوع شدن")
    refund_granted = models.BooleanField(default=False,verbose_name="مرجوع شده / نشده")

    def __str__(self):
        return self.user.username

    def get_total(self):
        total = 0
        for order_product in self.products.all():
            total += order_product.get_final_price()
        if self.coupon:
            total -= self.coupon.amount
        return total
    
    class Meta:
        verbose_name = "سفارش"
        verbose_name_plural ="سفارشات"

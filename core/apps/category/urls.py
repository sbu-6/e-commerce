from django.urls import path
from . import views

app_name = "category"

urlpatterns =[
    path("<slug>/",views.CategoryItem,name = 'category-detail'),
    path("<slug>/<page>/",views.CategoryItem,name = 'category-detail'),
]
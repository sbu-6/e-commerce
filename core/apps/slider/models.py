from django.db import models
import os

def get_file_ext(filepath):
    base_name = os.path.basename(filepath)
    name , ext = os.path.splitext(base_name)
    return  name , ext

def change_file_name(instance, filename):
    name,ext = get_file_ext(filename)
    new_name = f"SliderImage/{instance.id}-{instance.title}{ext}"
    return new_name



class Slider(models.Model):
    poster = models.ImageField(upload_to=change_file_name,default='image_default\default.jpg',verbose_name="پستر")
    title = models.CharField(max_length=100,blank=False,null=False,verbose_name="عنوان")
    detail = models.CharField(max_length=100,blank=False,null=False,verbose_name="جزئیات")
    description = models.TextField(verbose_name="توضیحات",null=False,blank=False)
    url = models.URLField(verbose_name="ادرس")
    title_url = models.CharField(max_length=30,blank=False,null=False,verbose_name="عنوان برای ادرس")
    
    def get_poster_url(self):
        return self.poster.url
    
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name="اسلاید"
        verbose_name_plural="اسلاید ها"
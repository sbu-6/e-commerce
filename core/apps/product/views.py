from django.core.paginator import Paginator , EmptyPage, PageNotAnInteger
from django.views.generic import ListView , DetailView
from django.views import View
from django.shortcuts import render,get_object_or_404,redirect
from order.forms import OrderAddProductForm as addproduct
from .models import Product



def ProductList(request,page=1,*args, **kwargs):
    page_number = page
    productlist = Product.objects.get_exists()
    try:
        paginator = Paginator(productlist, 16)
        products = paginator.get_page(page_number)
    except :
        products = ''

    context = {
        "title":"Home",
        'products': products,
        'page_obj':products,
    }
    return render(request, "work/Home.html", context)

def SearchProduct(request,*args,**kwargs):
    search = request.GET.get('search')
    if search == '':
        return redirect("product:Home")
    products = Product.objects.search(search)
    context = {
        "title":"search",
        'products': products,
    }
    return render(request, "work/Home.html", context)

def ProductDetail(request, *args, **kwargs):
    product = get_object_or_404(Product, slug=kwargs['slug'])
    form = addproduct(
        request.POST or None,
        initial={
            'product':product.id,
        }
    )
    context = {
        'product': product,
        'form':form,
    }
    return render(request, 'work/ProductDetail.html', context)


from django import forms
from django.core import validators


class BaseForm(forms.Form):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class OrderAddProductForm(forms.Form):
    product = forms.IntegerField(
        required=True,
        widget=forms.HiddenInput()
    )
    count = forms.IntegerField(
        label='',
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'min': 1,
                'value':1,
                "style":"width: 100px",
            }
        ),
        validators=[
            
        ]
    )
    
    def clean_count(self):
        count = self.cleaned_data.get('count')
        if count < 1:
            raise forms.ValidationError("count must be greater than 1")
        return count
    
    
    # def save(self):
    #     username = email = self.cleaned_data.get('email')
    #     first_name = self.cleaned_data.get('first_name')
    #     last_name = self.cleaned_data.get('last_name')
    #     password = self.cleaned_data.get('password')
    #     return User.objects.create_user(username, email=email, password=password,
    #                                 f   irst_name=first_name, last_name=last_name)
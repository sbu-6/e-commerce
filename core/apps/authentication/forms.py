from django.contrib.auth import login , authenticate
from django.shortcuts import redirect
from django.core import validators
from django.contrib.auth.models import BaseUserManager
from django import forms
from .models import User

class LoginForm(forms.Form):
    UserName = forms.EmailField(
        required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder':"User Name"
            }
        )
    )
    Password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control",
                "placeholder": "Password",
            }
        )
    )
    
    def clean_UserName(self):
        username = self.cleaned_data.get("UserName")
        get_user = User.objects.filter(email=username).exists()
        if not get_user:
            raise  forms.ValidationError("don't find user with this information!")
        return username
    def clean_Password(self):
        password = self.cleaned_data.get("Password")
        return password

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        return cleaned_data
    
    def LoginUser(self,request):
        username = self.cleaned_data.get("UserName")
        password = self.cleaned_data.get("Password")
        user = authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return True
        raise  forms.ValidationError("don't have user with this information!")
        

class SignUpForm(forms.Form):
    Firstname = forms.CharField(
        label="First name",
        required=True,
        widget=forms.TextInput(
            attrs={
                'class':"form-control",
                "placeholder":"First name"
            }
            
        )
    )
    Lastname = forms.CharField(
        label="First name",
        required=True,
        widget=forms.TextInput(
            attrs={
                'class':"form-control",
                "placeholder":"Last name"
            }
            
        )
    )
    username = forms.CharField(
        label="User name",
        required=True,
        widget=forms.TextInput(
            attrs={
                'class':"form-control",
                "placeholder":"User name"
            }
            
        )
    )
    phone = forms.CharField(
        label="Phone",
        required=True,
        widget=forms.NumberInput(
            attrs={
                "class":"form-control",
                "placeholder":"Phone"
            }
        )
    )
    email = forms.EmailField(
        label="Email address",
        required=True,
        widget=forms.EmailInput(
            attrs={
                "class":"form-control",
                "placeholder":"Email address"
            }
        )
    )
    password = forms.CharField(
        label="Password",
        required=True,
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control",
                "placeholder":"password"
            }
        )
    )
    re_password = forms.CharField(
        label="Re-Password",
        required=True,
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control",
                "placeholder":"Re-Password"
            }
        )
    )
    def clean_username(self):
        username = self.cleaned_data.get("username")
        get_user = User.objects.filter(username=username).exists()
        if get_user:
            raise forms.ValidationError("This User is already has been used!")
        return username
    
    def clean_email(self):
        email = self.cleaned_data.get("email")
        get_user = User.objects.filter(email=email).exists()
        if "@" not in email:
            raise forms.ValidationError("You'r email is not a valid email!")
        elif get_user:
            raise forms.ValidationError("This Email is already has been used!")
        return email
    
    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        return cleaned_data
    
    def clean_re_password(self):
        cleaned_data = super(SignUpForm, self).clean()
        password = cleaned_data.get("password")
        re_password = cleaned_data.get("re_password")
        if password != re_password:
            raise forms.ValidationError("Password and re_password is not matched!")
    
    def Save(self):
        cleaned_data = super(SignUpForm, self).clean()
        first_name = cleaned_data.get("Firstname")
        last_name = cleaned_data.get("Lastname")
        username = cleaned_data.get("username")
        email = cleaned_data.get("email")
        phone = cleaned_data.get("phone")
        password = cleaned_data.get("password")
        make_user = User.objects.create_new_user(
            username = username,
            first_name = first_name,
            last_name = last_name,
            phone = phone,
            email = email,
            password = password
        )
        if make_user:
            return True
        raise forms.ValidationError("Problem!")
    
class UserUpdateForm(forms.ModelForm):
    # TODO
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email','Phone_number']
        widgets = {
            "first_name": forms.TextInput(
                attrs={
                    "class":"form-control",
                }  
            ),
            "last_name": forms.TextInput(
                attrs={
                    "class":"form-control",
                }  
            ),
            "email":forms.EmailInput(
                attrs={
                    "class":"form-control",
                }
            ),
            "Phone_number": forms.TextInput(
                attrs={
                    "class":"form-control",
                }
            ),  
        }
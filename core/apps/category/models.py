from django.db import models
from django.db.models import Q
from django.urls import reverse

class CategoryManager(models.Manager):
    def get_active(self):
        return self.filter(is_active=True)
    def get_top(self):
        return self.get_queryset().filter(is_active=True,is_top=True)

class Category(models.Model):
    title = models.CharField(max_length=50,verbose_name='عنوان')
    slug = models.SlugField(verbose_name="لینک url")
    is_active = models.BooleanField(verbose_name="فعال/غیر فعال")
    is_top = models.BooleanField(verbose_name="بهترین دسته بندی ها")
    objects = CategoryManager()
    class Meta:
        verbose_name = 'دسته بندی'
        verbose_name_plural = 'دسته بندی ها'
        
    def get_absolute_url(self):
        return reverse('category:category-detail', kwargs={'slug': self.slug})

    def __str__(self):
        return f"{self.title}"


class TagManager(models.Manager):
    def get_active(self):
        return self.filter(is_active=True)
class Tag(models.Model):
    title = models.CharField(max_length=50,verbose_name='عنوان')
    is_active = models.BooleanField(verbose_name="فعال/غیر فعال")
    objects = TagManager()
    class Meta:
        verbose_name = 'تگ'
        verbose_name_plural = 'تگ ها'
    
    def __str__(self):
        return f"{self.title}"

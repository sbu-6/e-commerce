# Generated by Django 3.0 on 2021-01-23 19:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('category', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coupon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=15, verbose_name='کد')),
                ('amount', models.FloatField(verbose_name='میزان تخفیف')),
            ],
            options={
                'verbose_name': 'کپن',
                'verbose_name_plural': 'کپن ها',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('poster', models.ImageField(upload_to='ProductImage/', verbose_name='عکس')),
                ('slug', models.SlugField(verbose_name='لینک url')),
                ('title', models.CharField(max_length=100, verbose_name='عنوان')),
                ('price', models.FloatField(verbose_name='قیمت')),
                ('discount_price', models.FloatField(blank=True, null=True, verbose_name='تخفیف')),
                ('is_existent', models.BooleanField(default=True, verbose_name='موجود/ناموجود')),
                ('description', models.TextField(verbose_name='توضیحات')),
                ('category', models.ManyToManyField(to='category.Category', verbose_name='دسته بندی')),
                ('tag', models.ManyToManyField(to='category.Tag', verbose_name='تگ ها')),
            ],
            options={
                'verbose_name': 'محصول',
                'verbose_name_plural': 'محصولات',
            },
        ),
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='عنوان')),
                ('image', models.ImageField(upload_to='Gallry/', verbose_name='عکس')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gallery', to='product.Product', verbose_name='محصول')),
            ],
            options={
                'verbose_name': 'گالری',
                'verbose_name_plural': 'گالری',
            },
        ),
    ]

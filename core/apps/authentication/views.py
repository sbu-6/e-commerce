from django.shortcuts import render , redirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .models import User
from .decorators import IsAuthenticated
from .forms import LoginForm , SignUpForm
from django.contrib import messages
from order.models import Order , Orderproduct

@IsAuthenticated
def LoginPage(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        if form.LoginUser(request):
            return redirect('product:Home')  
    context ={
        "title":"Login",
        "form": form,
    }
        
    return render(request,'auth/SignIn.html',context)


@login_required(login_url="profile:Login")
def LogoutPage(request):
    logout(request)
    return redirect('product:Home')


@IsAuthenticated
def SignUpPage(request):
    form = SignUpForm(request.POST or None)
    if form.is_valid():
        if form.Save():
            return redirect('product:Home')
    context = {
        "title":"Sign Up",
        "form":form
    }
    return render(request,'auth/SignUp.html',context)

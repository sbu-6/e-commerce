from django import forms
from .models import Address

class AddressForm(forms.Form):
    user = forms.IntegerField(
        required=False,
        widget = forms.HiddenInput()
    )
    state_address = forms.CharField(
        required = True,
        widget = forms.TextInput(
            attrs={
                "class": "form-control",
                # "placeholder": "state"
            }
        )
    )
    city_address = forms.CharField(
        required = True,
        widget = forms.TextInput(
            attrs={
                "class": "form-control",
                # "placeholder": "city"
            }
        )
    )
    zip_code = forms.CharField(
        required = True,
        widget = forms.TextInput(
            attrs={
                "class": "form-control",
                # "placeholder": "zip code"
            }
        )
    )
    default = forms.BooleanField(
        widget = forms.CheckboxInput(
            attrs={
                "class": "form-check-input",
            }
        )
    )
    detail_address = forms.CharField(
        required=True,
        widget=forms.Textarea(
            attrs={
                "class":"form-control",
                "placeholder":"Address ",
                "rows":1,
            }
        )
    )
    
    def Save(self):
        all_data = self.cleaned_data()
        print(all_data)
    # def __init__(self,*args,**kwargs):
    #     super().__init__()
    #     print(self.fields['zip_code'])

class AddAddressForm(forms.ModelForm):

    class Meta:
        model = Address
        fields =[]
        widgets = {
            "user" : forms.HiddenInput(),
            "state_address":forms.TextInput(
            attrs={
                "class": "form-control",
                # "placeholder": "state"
                }
            ),
            "city_address":forms.TextInput(
            attrs={
                "class": "form-control",
                # "placeholder": "state"
                }
            ),
            "detail_address":forms.Textarea(
            attrs={
                "class":"form-control",
                "placeholder":"Address ",
                "rows":1,
            }
            ),
            "default":forms.CheckboxInput(
            attrs={
                "class": "form-check-input",
            }
            ),
            "zip_code":forms.TextInput(
            attrs={
                "class": "form-control",
                # "placeholder": "state"
                }
            ),
        }
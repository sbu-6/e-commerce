from django.shortcuts import render,redirect,get_object_or_404,HttpResponse,Http404
from django.contrib.auth.decorators import login_required
from .forms import OrderAddProductForm
from .models import Order, Orderproduct
from product.models import Product
from django.utils import timezone


@login_required(login_url='profile:Login')
def AddProductToOrder(request):
    if request.POST:
        product_id = request.POST.get('product')
        product_count = request.POST.get('count')
        product = get_object_or_404(Product,id=int(product_id))
        order_product, created = Orderproduct.objects.get_or_create(
            user = request.user,
            ordered = False,
            product = product,
            quantity = product_count
        )
        order_qs = Order.objects.filter(user = request.user , ordered = False)
        if order_qs.exists():
            order = order_qs.first()
            if order.products.filter(product__slug=product.slug).exists():
                order_product.quantity += product_count
                order_product.save()
                # redirect to page product detail
                return redirect("product:product-detail",product.slug)
            else:
                order.products.add(order_product)
                # redirect to page product detail
                return redirect("product:product-detail",product.slug)
        else:
            order_date = timezone.now()
            order = Order.objects.create(
                user = request.user,
                ordered_date = order_date
            )
            order.products.add(order_product)
            # redirect to page product detail
            return redirect("product:product-detail",product.slug)
    return Http404("Problem!")

@login_required(login_url='profile:Login')
def OrederDetail(request):
    context ={
        "title":"orders",
        'orders':None,
           
    }
    user = request.user
    orders = Order.objects.filter(
        user = user,
        ordered=False,
    ).first()
    context['orders'] = orders

    return render(request,"work/OrderDetail.html",context)
        
@login_required(login_url="profile:Login")
def DeleteOrederProduct(request,pk):
    order_detail = Orderproduct.objects.get(id=pk,user=request.user)
    if order_detail is not None:
        order_detail.delete()
        return redirect("profile:dashboard")
    return Http404()



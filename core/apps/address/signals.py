from django.db.models.signals import pre_save
from .models import Address
from django.dispatch import receiver

# @receiver(pre_save,sender=Address)
def update_address(sender,instance, **kwargs):
    print(kwargs)
    if instance.default:
        print(instance)
        objs = Address.objects.filter(user=instance.user,default = True)
        for obj in objs:
            obj.default = False
            obj.save()
pre_save.connect(update_address, sender=Address)
                
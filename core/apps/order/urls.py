from django.urls import path
from . import views

app_name = 'order'
urlpatterns =[
    path("AddProductToOrder",views.AddProductToOrder,name="AddProduct"),
    path("OrderDetail/",views.OrederDetail,name="order-detail"),
    path("DeleteOrderProduct/<pk>/",views.DeleteOrederProduct,name="DeleteOrderProduct"),
]
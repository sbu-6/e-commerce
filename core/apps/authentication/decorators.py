from django.shortcuts import redirect,Http404

class IsAuthenticated(object):
    def __init__(self,func,*args,**kwargs):
        self.func = func
    def __call__(self,request,*args,**kwargs):
        if not request.user.is_authenticated:
            return self.func(request,*args,**kwargs)
        return redirect('product:Home')
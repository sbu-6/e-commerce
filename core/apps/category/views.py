from django.core.paginator import Paginator , EmptyPage, PageNotAnInteger
from django.shortcuts import render , get_object_or_404
from .models import Category
# Create your views here.

def CategoryItem(request,slug,page=1):
    category= get_object_or_404(Category,slug=slug)
    productlist = category.product.get_exists()
    try:
        paginator = Paginator(productlist, 16)
        products = paginator.get_page(page)
    except :
        products = ''
    context = {
        "title":category.title,
        "products":products
    }
    return render(request, "work/Home.html", context)